export default [
    { name: 'Home', link: '/home', icon: 'fas fa-home' },
    { name: 'Dashboards', link: '/dashboards', icon: 'fas fa-chart-bar' },
    { name: 'Search', link: '/search', icon: 'fas fa-search' },
    { name: 'Automated Workflows', link: '/workflows', icon: 'fas fa-server' },
    { name: 'Data Sources', link: '/sources', icon: 'fas fa-database' },
    { name: 'Exports', link: '/exports', icon: 'fas fa-external-link-alt' },
    { name: 'Custom Rules', link: '/rules', icon: 'fas fa-filter' },
    { name: 'System', link: '/system', icon: 'fas fa-window-restore' },
];
