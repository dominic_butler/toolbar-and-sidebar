import React from 'react';
import regeneratorRuntime from 'regenerator-runtime';
import { render, screen } from '@testing-library/react';
import Checkbox from './Checkbox';
import '@testing-library/jest-dom/extend-expect';
import { ThemeProvider } from 'styled-components';
import theme from '../../styledComponentsTheme';

test('render Checkbox', () => {
    render(
        <ThemeProvider theme={theme}>
            <Checkbox label='checkbox' checked onChange={jest.fn()} />
        </ThemeProvider>
    );
    expect(screen.getByText('checkbox')).toBeInTheDocument;
});
