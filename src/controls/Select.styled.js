import styled, { css } from 'styled-components';

export const StyledSelect = styled.label`
    ${({ disabled, theme, hasError }) => css`
        height: 34px;
        background: ${theme.color.base[0]};
        border-radius: 4px;
        border-width: 1px;
        border-style: solid;
        border-color: ${theme.color.base[3]};
        display: flex;
        position: relative;
        box-sizing: border-box;
        font-weight: normal;
        overflow: hidden;
        border-color: ${hasError
            ? theme.color.feedback.negative
            : theme.color.base[3]};
        & span {
            text-transform: capitalize;
            background: ${theme.color.base[1]};
            border-right: 1px solid ${theme.color.base[3]};
            height: 100%;
            display: flex;
            align-items: center;
            padding: 0 10px;
            pointer-events: none;
        }
        & select {
            height: 100%;
            flex: 1;
            padding: 0 30px 0 10px;
            border: none;
            background: transparent;
            -webkit-appearance: none;
            &::-ms-expand {
                display: none;
            }
            &:active,
            &:focus {
                outline: 0;
            }
        }
        ${disabled &&
        css`
            background: ${theme.color.base[2]};
            cursor: not-allowed;
            & select,
            & label {
                pointer-events: none;
            }
        `}
        & .drop-icon {
            position: absolute;
            pointer-events: none;
            top: 0;
            right: 10px;
            height: 100%;
            display: flex;
            align-items: center;
        }
    `}
`;
