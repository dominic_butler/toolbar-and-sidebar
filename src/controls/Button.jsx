import React from 'react';
import PropTypes from 'prop-types';
import { StyledButton } from './Button.styled';
import TooltipV2 from './TooltipV2.jsx';

const Button = ({
    testId,
    type,
    buttonColor,
    buttonText,
    onClick,
    disabled,
    buttonIcon,
    style,
    id,
    className,
    isVisible,
    tooltip,
}) =>
    isVisible && (
        <>
            <StyledButton
                data-testid={testId}
                onClick={
                    onClick
                        ? e => {
                              e.preventDefault();
                              onClick();
                          }
                        : null
                }
                buttonColor={buttonColor}
                disabled={disabled}
                style={style}
                id={id}
                type={type}
                buttonText={buttonText}
                className={className}
                isVisible={isVisible}
            >
                {tooltip && id && (
                    <TooltipV2
                        attachToId={id}
                        position={tooltip.position}
                        message={tooltip.message}
                    />
                )}
                {buttonText}
                {buttonIcon && <i className={buttonIcon} />}
            </StyledButton>
        </>
    );

Button.propTypes = {
    buttonColor: PropTypes.string,
    buttonText: PropTypes.string,
    title: PropTypes.string,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    buttonIcon: PropTypes.any,
    style: PropTypes.object,
    tooltip: PropTypes.object,
    id: PropTypes.string,
    type: PropTypes.string,
    isVisible: PropTypes.bool,
    testId: PropTypes.string,
};

Button.defaultProps = {
    buttonColor: 'default',
    isVisible: true,
};

export default Button;
