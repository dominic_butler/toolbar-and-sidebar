import styled, { css } from 'styled-components';
import color from 'color';

export const StyledCheckbox = styled.div`
    ${({ theme }) => css`
        height: 22px;
        position: relative;
        & > input {
            display: none;
            & + label:after {
                content: none;
            }
            &:disabled + label {
                cursor: not-allowed;
            }
            &:disabled + label:before {
                background: ${theme.color.base[3]};
            }
            &:checked {
                & + label:before {
                    background: ${theme.color.accent.primary};
                    border: 1px solid
                        ${color(theme.color.accent.primary).darken(0.1).hex()};
                }
                &:disabled + label:before {
                    background: ${theme.color.base[2]};
                    border: 1px solid ${theme.color.base[4]};
                }
                & + label:after {
                    content: '';
                }
            }
        }
        & > label {
            cursor: pointer;
            position: relative;
            padding-left: 30px;
            user-select: none;
            display: flex;
            align-items: center;
            height: 100%;
            font-weight: normal;
            &:before {
                background: ${theme.color.base[0]};
                content: '';
                position: absolute;
                left: 0;
                top: 0;
                display: inline-block;
                height: 20px;
                width: 20px;
                border: 1px solid ${theme.color.base[4]};
                border-radius: 4px;
            }
            &:after {
                content: '';
                position: absolute;
                left: 4px;
                top: 5px;
                display: inline-block;
                height: 7px;
                width: 13px;
                border-left: 2px solid ${theme.color.base[0]};
                border-bottom: 2px solid ${theme.color.base[0]};
                transform: rotate(-45deg);
            }
        }
    `}
`;
