import React from 'react';
import PropTypes from 'prop-types';
import { StyledCheckbox } from './Checkbox.styled';

const Checkbox = ({ checked, onChange, disabled, label, id, style }) => {
    const randomID = 'checkbox-' + Math.random();
    return (
        <StyledCheckbox style={style} id={id}>
            <input
                type='checkbox'
                id={randomID}
                disabled={disabled}
                onChange={onChange}
                checked={checked}
            />
            <label htmlFor={randomID}>{label}</label>
        </StyledCheckbox>
    );
};

Checkbox.propTypes = {
    checked: PropTypes.bool,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    id: PropTypes.string,
    label: PropTypes.string,
    style: PropTypes.object,
};

export default Checkbox;
