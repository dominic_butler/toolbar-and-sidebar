import styled, { css } from 'styled-components';

export const StyledTextInput = styled.label`
    ${({ disabled, theme, hasError }) => css`
        display: flex;
        align-items: center;
        white-space: nowrap;
        border-radius: 4px;
        background: ${theme.color.base[0]};
        height: 34px;
        box-sizing: border-box;
        font-weight: normal;
        overflow: hidden;
        border: 1px solid
            ${hasError ? theme.color.feedback.negative : theme.color.base[3]};
        ${disabled &&
        css`
            background: ${theme.color.base[2]};
            color: ${theme.color.base[4]};
            cursor: not-allowed;
            & > input {
                pointer-events: none;
            }
        `};
        & > input {
            padding: 0 10px;
            border: none;
            flex: 1;
            height: 100%;
            background: none;
            &::placeholder {
                color: ${theme.color.base[4]};
            }
            &:active,
            &:focus {
                border: none;
                outline: none;
            }
        }
        & > span {
            text-transform: capitalize;
            padding: 0 10px;
            background: ${theme.color.base[1]};
            border-right: 1px solid ${theme.color.base[3]}
            height: 100%;
            display: flex;
            align-items: center;
            pointer-events: none;
            user-select: none;
        }
    `}
`;
