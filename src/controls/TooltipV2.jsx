import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { StyledTooltip } from './Tooltip.styled';

function Tooltip({ attachToId, position, message }) {
    const [isVisible, setIsVisible] = useState(false);
    useEffect(() => handleVisibility(), []);

    const show = () => setIsVisible(true);
    const hide = () => setIsVisible(false);

    const handleVisibility = () => {
        const element = document.getElementById(attachToId);
        element.addEventListener('mouseenter', show);
        element.addEventListener('mouseleave', hide);
        return () => {
            element.removeEventListener('mouseenter', show);
            element.removeEventListener('mouseleave', hide);
        };
    };

    const returnPosition = () => {
        const element = document.getElementById(attachToId);
        const rect = element.getBoundingClientRect();

        const yTop = rect.top;
        const yMid = rect.bottom - (rect.bottom - rect.top) / 2;
        const yLow = rect.bottom;
        const xLeft = rect.left;
        const xMid = rect.right - (rect.right - rect.left) / 2;
        const xRight = rect.right;

        switch (position) {
            case 'top':
                return [xMid, yTop, 'translate(-50%, -100%)'];
            case 'top-left':
                return [xLeft, yTop, 'translate(0, -100%)'];
            case 'top-right':
                return [xRight, yTop, 'translate(-100%, -100%)'];
            case 'right':
                return [xRight, yMid, 'translate(0, -50%)'];
            case 'bottom':
                return [xMid, yLow, 'translate(-50%, 0)'];
            case 'bottom-left':
                return [xLeft, yLow, 'translate(0, 0)'];
            case 'bottom-right':
                return [xRight, yLow, 'translate(-100%, 0)'];
            case 'left':
                return [xLeft, yMid, 'translate(-100%, -50%)'];
            default:
                return [xMid, yLow, 'translate(-50%, 0)'];
        }
    };

    return (
        isVisible && (
            <StyledTooltip position={returnPosition()}>
                <div>{message}</div>
            </StyledTooltip>
        )
    );
}

Tooltip.propTypes = {
    attachToId: PropTypes.string,
    message: PropTypes.string,
    position: PropTypes.string,
};

Tooltip.defaultProps = {
    message: 'tooltip',
};

export default Tooltip;
