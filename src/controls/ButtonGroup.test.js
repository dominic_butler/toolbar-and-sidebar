import React from 'react';
import regeneratorRuntime from 'regenerator-runtime';
import { render, screen } from '@testing-library/react';
import ButtonGroup from './ButtonGroup';
import '@testing-library/jest-dom/extend-expect';

test('render a button group with fake buttons', () => {
    render(
        <ButtonGroup>
            <button>button 1</button>
            <button>button 2</button>
            <button>button 3</button>
        </ButtonGroup>
    );
    expect(screen.getByText('button 1')).toBeInTheDocument;
    expect(screen.getByText('button 2')).toBeInTheDocument;
    expect(screen.getByText('button 3')).toBeInTheDocument;
});
