import styled, { css } from 'styled-components';
import color from 'color';

const returnButtonColors = (background, color, border, hover) =>
    css`
        background: ${background};
        color: ${color};
        border: 1px solid ${border};
        &:not(:disabled):hover {
            background: ${hover};
        }
    `;

export const StyledButton = styled.button`
    padding: 6px 12px;
    border-radius: 4px;
    font-size: 14px;
    text-transform: capitalize;
    border: none;
    height: 34px;
    white-space: nowrap;
    text-decoration: none;
    cursor: pointer;
    ${({ buttonColor, theme }) =>
        (buttonColor === 'default' &&
            returnButtonColors(
                theme.color.base[2],
                theme.color.base[5],
                theme.color.base[3],
                theme.color.base[3]
            )) ||
        (buttonColor === 'primary' &&
            returnButtonColors(
                theme.color.accent.primary,
                theme.color.base[0],
                color(theme.color.accent.primary).darken(0.1).hex(),
                color(theme.color.accent.primary).darken(0.1).hex()
            )) ||
        (buttonColor === 'secondary' &&
            returnButtonColors(
                theme.color.accent.secondary,
                theme.color.base[0],
                color(theme.color.accent.secondary).darken(0.1).hex(),
                color(theme.color.accent.secondary).darken(0.1).hex()
            )) ||
        (buttonColor === 'warning' &&
            returnButtonColors(
                theme.color.feedback.warning,
                theme.color.base[0],
                color(theme.color.feedback.warning).darken(0.1).hex(),
                color(theme.color.feedback.warning).darken(0.1).hex()
            )) ||
        (buttonColor === 'danger' &&
            returnButtonColors(
                theme.color.feedback.negative,
                theme.color.base[0],
                color(theme.color.feedback.negative).darken(0.1).hex(),
                color(theme.color.feedback.negative).darken(0.1).hex()
            )) ||
        (buttonColor === 'success' &&
            returnButtonColors(
                theme.color.feedback.positive,
                theme.color.base[0],
                color(theme.color.feedback.positive).darken(0.1).hex(),
                color(theme.color.feedback.positive).darken(0.1).hex()
            )) ||
        (buttonColor === 'link' &&
            css`
                background-color: transparent;
                color: ${theme.color.accent.primary};
                white-space: pre-wrap;
                &:hover {
                    text-decoration: underline;
                }
            `)}
    & > svg {
        margin-left: ${({ buttonText }) => (buttonText ? '8px' : 0)};
    }
    &:active,
    &:focus {
        outline: 0;
    }
    &:disabled {
        cursor: not-allowed;
        opacity: 0.7;
        ${({ buttonColor }) =>
            buttonColor === 'link' &&
            css`
                &:hover {
                    text-decoration: none;
                }
            `}
    }
`;
