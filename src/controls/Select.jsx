import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { StyledSelect } from './Select.styled.js';

const Select = ({
    tooltip,
    tooltipAlignment,
    label,
    hasError,
    nameFormatter,
    valueFormatter,
    style,
    id,
    value,
    onChange,
    disabled,
    placeholder,
    initialOption,
    clearFilterOption,
    options,
    optionValue,
    optionName,
    optionDisabled,
}) => (
    <StyledSelect
        id={id}
        hasError={hasError}
        disabled={disabled}
        style={style}
        data-tooltip={tooltip}
        data-tooltip-alignment={tooltipAlignment}
    >
        {label && <span>{label}</span>}
        <select
            value={value}
            onChange={onChange}
            disabled={disabled}
            placeholder={placeholder}
            data-tooltip={tooltip}
            data-tooltip-alignment={tooltipAlignment}
        >
            {initialOption && (!value || !value.length) ? (
                <option>{initialOption}</option>
            ) : null}
            {clearFilterOption && <option>All</option>}
            {options &&
                options.map((option, i) => (
                    <option
                        disabled={_.get(option, optionDisabled, false)}
                        key={i}
                        value={
                            valueFormatter
                                ? valueFormatter(option)
                                : _.get(option, optionValue, false)
                        }
                    >
                        {nameFormatter
                            ? nameFormatter(option)
                            : _.get(option, optionName, false)}
                    </option>
                ))}
        </select>
        <div className='drop-icon'>
            <i className='fas fa-chevron-down' />
        </div>
    </StyledSelect>
);

Select.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    options: PropTypes.array,
    label: PropTypes.string,
    tooltip: PropTypes.string,
    tooltipAlignment: PropTypes.string,
    style: PropTypes.object,
    disabled: PropTypes.bool,
    hasError: PropTypes.bool,
    onChange: PropTypes.func,
    optionName: PropTypes.string,
    optionKeyProp: PropTypes.string,
    optionValue: PropTypes.string,
    optionDisabled: PropTypes.bool,
    title: PropTypes.string,
    placeholder: PropTypes.string,
    initialOption: PropTypes.string,
    clearFilterOption: PropTypes.bool,
    id: PropTypes.string,
    nameFormatter: PropTypes.func,
    valueFormatter: PropTypes.func,
};

Select.defaultProps = {
    optionName: 'name',
    optionValue: 'value',
};

export default Select;
