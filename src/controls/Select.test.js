import React from 'react';
import regeneratorRuntime from 'regenerator-runtime';
import { render, screen } from '@testing-library/react';
import Select from './Select';
import '@testing-library/jest-dom/extend-expect';
import { ThemeProvider } from 'styled-components';
import theme from '../../styledComponentsTheme';
import userEvent from '@testing-library/user-event';

jest.mock('@fortawesome/react-fontawesome', () => ({
    FontAwesomeIcon: jest.fn(() => <i>icon</i>),
}));

const options = [
    { value: 'one', name: 'one' },
    { value: 'two', name: 'two' },
    { value: 'three', name: 'three' },
];

test('render Select', () => {
    render(
        <ThemeProvider theme={theme}>
            <Select
                label='select'
                options={options}
                initialOption='one'
                clearFilterOption
            />
        </ThemeProvider>
    );
    expect(screen.getByText('select')).toBeInTheDocument;
});

test('render Select with value/name formatter', () => {
    render(
        <ThemeProvider theme={theme}>
            <Select
                label='select'
                options={options}
                valueFormatter={x => x}
                nameFormatter={jest.fn()}
            />
        </ThemeProvider>
    );
    expect(screen.getByText('select')).toBeInTheDocument;
});
