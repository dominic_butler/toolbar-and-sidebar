import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledButtonGroup = styled.div`
    display: inline-flex;
    white-space: nowrap;
    & input,
    button,
    textarea,
    label {
        border-radius: 0;
        flex-shrink: 0;
    }
    & :first-child {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }
    & :last-child {
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
    }
    & :not(:last-child) {
        border-right: none !important;
    }
`;

const ButtonGroup = ({ children, style, id }) => (
    <StyledButtonGroup style={style} id={id || ''}>
        {children}
    </StyledButtonGroup>
);

ButtonGroup.propTypes = {
    id: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.any,
};

export default ButtonGroup;
