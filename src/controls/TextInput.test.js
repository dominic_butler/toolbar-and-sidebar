import React from 'react';
import regeneratorRuntime from 'regenerator-runtime';
import { render, screen } from '@testing-library/react';
import TextInput from './TextInput';
import '@testing-library/jest-dom/extend-expect';
import { ThemeProvider } from 'styled-components';
import theme from '../../styledComponentsTheme';

test('render TextInput', () => {
    render(
        <ThemeProvider theme={theme}>
            <TextInput label='text input' />
        </ThemeProvider>
    );
    expect(screen.getByText('text input')).toBeInTheDocument;
});
