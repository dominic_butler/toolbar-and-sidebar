import styled, { css } from 'styled-components';

export const StyledTooltip = styled.div`
    ${({ position, theme }) => css`
        position: absolute;
        pointer-events: none;
        left: ${position[0]}px;
        top: ${position[1]}px;
        transform: ${position[2]};
        z-index: 100000;
        & > div {
            background: ${theme.color.base[0]};
            font-size: 13px;
            color: ${theme.color.base[5]};
            box-shadow: 0 1px 5px rgba(0, 0, 0, 0.2);
            padding: 5px 10px;
            border-radius: 4px;
            max-width: 300px;
            text-align: left;
            margin: 5px;
        }
    `}
`;
