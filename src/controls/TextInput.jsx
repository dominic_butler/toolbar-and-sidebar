import React from 'react';
import PropTypes from 'prop-types';
import { StyledTextInput } from './TextInput.styled';

const TextInput = ({
    testId,
    tooltip,
    tooltipAlignment,
    label,
    hasError,
    maxlength,
    required,
    autoComplete,
    placeholder,
    id,
    onFocus,
    onBlur,
    disabled,
    value,
    type,
    style,
    onChange,
    min,
    max,
    onKeyPress,
}) => (
    <StyledTextInput
        data-tooltip={tooltip}
        data-tooltip-alignment={tooltipAlignment}
        id={id}
        style={style}
        hasError={hasError}
        disabled={disabled}
    >
        {label && <span>{label}</span>}
        <input
            data-testid={testId}
            disabled={disabled}
            value={value}
            onBlur={onBlur}
            type={type}
            min={min}
            max={max}
            onFocus={onFocus}
            onChange={onChange}
            onKeyPress={onKeyPress}
            placeholder={placeholder}
            autoComplete={autoComplete}
            maxLength={maxlength}
            required={required}
        />
    </StyledTextInput>
);

TextInput.propTypes = {
    label: PropTypes.string,
    id: PropTypes.string,
    type: PropTypes.string,
    tooltip: PropTypes.string,
    tooltipAlignment: PropTypes.string,
    min: PropTypes.number,
    max: PropTypes.number,
    placeholder: PropTypes.string,
    style: PropTypes.object,
    onChange: PropTypes.func,
    onKeyPress: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    disabled: PropTypes.bool,
    hasError: PropTypes.bool,
    required: PropTypes.bool,
    autoComplete: PropTypes.string,
    maxlength: PropTypes.string,
    testId: PropTypes.string,
};

TextInput.defaultProps = {
    autoComplete: 'off',
};

export default TextInput;
