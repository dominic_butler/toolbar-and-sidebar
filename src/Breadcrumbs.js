import React from 'react';
import * as styled from './Breadcrumbs.styled';

export default function Breadcrumbs({ root }) {
    return (
        <styled.Breadcrumbs>
            {root}
            <i class='fas fa-caret-right' />
            Breadcrumb
            <i class='fas fa-caret-right' />
            Breadcrumb
            <i class='fas fa-caret-right' />
            Breadcrumb
            <i class='fas fa-caret-right' />
            Breadcrumb
        </styled.Breadcrumbs>
    );
}
