import styled from 'styled-components';

export const App = styled.div`
    display: flex;
    height: 100vh;
    width: 100vw;
`;

export const Main = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
`;

export const Content = styled.div`
    flex: 1;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    font-size: 150px;
    color: silver;
`;
