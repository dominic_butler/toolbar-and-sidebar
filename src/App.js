import * as styled from './App.styled';
import { Link, Route, Switch } from 'react-router-dom';
import Sidebar from './Sidebar';
import data from './App.data';
import Toolbar from './Toolbar';
import Breadcrumbs from './Breadcrumbs';

export default function App() {
    return (
        <styled.App className='App'>
            <Sidebar />
            <styled.Main>
                {data.map(d => (
                    <Route path={d.link}>
                        <Toolbar />
                        <styled.Content>
                            <i class={d.icon}></i>
                        </styled.Content>
                        <Breadcrumbs root={d.name} />
                    </Route>
                ))}
            </styled.Main>
        </styled.App>
    );
}
