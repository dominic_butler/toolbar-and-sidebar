import * as styled from './Sidebar.styled';
import data from './App.data';
import { useLocation } from 'react-router-dom';
export default function Sidebar() {
    const location = useLocation();
    return (
        <styled.Sidebar>
            <styled.Logo>
                <img src='/exonar_web_logo_white.png' />
            </styled.Logo>
            <div className='menu-items'>
                {data.map(d => (
                    <styled.MenuItem
                        to={d.link}
                        active={location.pathname === d.link}
                    >
                        <div>
                            <i class={d.icon}></i>
                        </div>
                        {d.name}
                    </styled.MenuItem>
                ))}
            </div>
            <styled.UserArea>
                <styled.UserTile>
                    <i class='fas fa-sign-out-alt'></i>
                    <span>Logout</span>
                </styled.UserTile>
                <styled.UserTile>
                    <i class='fas fa-user-cog'></i>
                    <span>Settings</span>
                </styled.UserTile>
                <styled.UserTile>
                    <i class='fas fa-unlock'></i>
                    <span>Password</span>
                </styled.UserTile>
            </styled.UserArea>
        </styled.Sidebar>
    );
}
