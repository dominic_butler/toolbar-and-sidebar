import styled from 'styled-components';

export const Breadcrumbs = styled.div`
    background-color: ${props => props.theme.color.base[1]};
    border-top: 1px solid ${props => props.theme.color.base[3]};
    padding: 7px 15px 10px 15px;
    display: flex;
    z-index: 30000;
    font-size: 15px;
    align-items: center;
    & .fas {
        padding: 0 15px;
        color: ${props => props.theme.color.base[4]};
    }
`;
