export default {
    color: {
        base: [
            '#ffffff',
            '#F4F4F4',
            '#E6E6E6',
            '#CDCDCD',
            '#727272',
            '#4D4D4D',
        ],
        accent: {
            primary: '#4792DC',
            secondary: '#da1884',
        },
        feedback: {
            negative: '#DC3544',
            warning: '#EC971E',
            positive: '#28A745',
        },
    },
};
