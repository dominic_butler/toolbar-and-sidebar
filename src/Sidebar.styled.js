import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Sidebar = styled.div`
    width: 270px;
    background-color: ${props => props.theme.color.base[5]};
    display: flex;
    flex-direction: column;
    & .menu-items {
        flex: 1;
    }
`;

export const Logo = styled.div`
    padding: 15px 0;
    & img {
        width: 180px;
    }
    text-align: center;
    border-bottom: 1px solid ${props => props.theme.color.base[4]};
`;

export const MenuItem = styled(Link)`
    display: flex;
    text-decoration: none;
    color: white;
    align-items: center;
    font-size: 15px;
    background-color: ${props =>
        props.active ? 'rgba(255, 255, 255, 0.1)' : 'transparent'};
    & > div {
        background-color: ${props =>
            props.active ? props.theme.color.accent.secondary : 'transparent'};
        width: 55px;
        height: 50px;
        font-size: 20px;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-right: 7px;
    }
    &:hover {
        background-color: rgba(255, 255, 255, 0.1);
    }
`;

export const UserArea = styled.div`
    display: flex;
    height: 80px;
    border-top: 1px solid ${props => props.theme.color.base[4]};
`;

export const UserTile = styled(Link)`
    color: ${props => props.theme.color.base[0]};
    display: flex;
    flex-direction: column;
    flex: 1;
    justify-content: center;
    align-items: center;
    font-size: 20px;
    text-decoration: none;
    cursor: pointer;
    & > span {
        font-size: 14px;
        margin-top: 5px;
    }
    &:hover {
        background-color: rgba(255, 255, 255, 0.1);
    }
`;
