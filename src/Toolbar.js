import React from 'react';
import * as styled from './Toolbar.styled';
import Button from './controls/Button';
import ButtonGroup from './controls/ButtonGroup';
import Select from './controls/Select';

export default function Toolbar() {
    return (
        <styled.Toolbar>
            <div>
                <ButtonGroup>
                    <Button buttonIcon='fas fa-chart-pie' />
                    <Button buttonIcon='fas fa-user-shield' />
                    <Button buttonIcon='fas fa-exclamation-triangle' />
                </ButtonGroup>
                <styled.Label>Dashboard</styled.Label>
            </div>
            <styled.Divider />
            <div>
                <Select
                    label='sort by'
                    options={[
                        { value: 'File Count', name: 'File Count' },
                        { value: 'File Size', name: 'File Size' },
                        { value: 'Storage Cost', name: 'Storage Cost' },
                    ]}
                />
                <styled.Label>Charts</styled.Label>
            </div>
            <styled.Divider />
            <div>
                <Button buttonText='Show' style={{ marginRight: 5 }} />
                <Button buttonIcon='fas fa-cog' />
                <styled.Label>Table</styled.Label>
            </div>
            <styled.Divider />
            <div>
                <Button buttonIcon='fas fa-external-link-alt' />
                <styled.Label>Export</styled.Label>
            </div>
            <div style={{ flex: 1 }}></div>
            <div>
                <Button
                    buttonIcon='fas fa-chalkboard-teacher'
                    style={{ marginRight: 5 }}
                />
                <Button buttonIcon='fas fa-question' />
                <styled.Label>Help</styled.Label>
            </div>
            <styled.Divider />
            <div style={{ textAlign: 'center' }}>
                <Button buttonIcon='fas fa-bell' buttonText='5' />
                <styled.Label>Notifications</styled.Label>
            </div>
        </styled.Toolbar>
    );
}
