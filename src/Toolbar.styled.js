import styled from 'styled-components';

export const Toolbar = styled.div`
    background-color: ${props => props.theme.color.base[1]};
    border-bottom: 1px solid ${props => props.theme.color.base[3]};
    padding: 10px 10px 7px 10px;
    display: flex;
    z-index: 30000;
`;

export const Label = styled.div`
    font-size: 12px;
    text-align: center;
    margin-top: 5px;
`;

export const Divider = styled.div`
    min-height: 100%;
    border-right: 1px solid ${props => props.theme.color.base[3]};
    margin: 0 15px;
`;
